import axios from "axios";
import { ENV } from "../utils/constants";




const getproducts =  async () => {
    return axios.get(`${ENV.API_URL}/${ENV.ENDPOINTS.PRODUCTS}`
);
}


const newproduct =  async (name, price, category, imgURL) => {
    return axios.post(`${ENV.API_URL}/${ENV.ENDPOINTS.PRODUCTS}`,
    {
        name : name,
        price : price,
        category : category,
        imgURL : imgURL
    }
);
}
const deleteProduct = async (productId) => {
    return axios.delete(`${ENV.API_URL}/${ENV.ENDPOINTS.PRODUCTS}/${productId}`);
};
const editProduct = async (productId, name, price, category, imgURL) => {
    return axios.put(`${ENV.API_URL}/${ENV.ENDPOINTS.PRODUCTS}/${productId}`, {
        name: name,
        price: price,
        category: category,
        imgURL: imgURL
    });
}


export default {
    getproducts,newproduct,deleteProduct,editProduct
}