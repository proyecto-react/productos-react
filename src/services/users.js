import {jwtDecode} from "jwt-decode";
import { ENV } from "../utils/constants";
import { authFetch } from "../utils/authFetch";

const getMe = async (token) => {
    try {
        // Verificar si el token está presente
        if (!token) throw new Error("Token is required");

        // Decodificar el token
        const decoded = jwtDecode(token);

        // Verificar si el ID de usuario está presente en el token decodificado
        const userId = decoded._id;
        if (!userId) throw new Error("Invalid token: User ID not found");

        // Construir la URL y realizar la solicitud
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.USER}/${userId}`;
        const response = await authFetch(url);

        // Verificar la respuesta
        if (!response.ok) throw new Error(`Failed to fetch user data: ${response.statusText}`);

        return response.json();
    } catch (error) {
        console.error("Error in getMe:", error);
        throw error; // Rethrow para que el llamador pueda manejar el error
    }
}

const editUser = async (token, userData) => {
    try {
        
        const url = `${ENV.API_URL}/${ENV.ENDPOINTS.PROFILE}/${token}`;
        const response = await authFetch(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(userData),
        });
        return response.json();
    } catch (error) {
        console.log(error);
    }
};

export const userService = {
    getMe,
    editUser,
};
