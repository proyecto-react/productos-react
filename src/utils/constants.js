export const ENV = {
    API_URL: 'https://api-rest-products-teal.vercel.app',
    ENDPOINTS: {
        LOGIN:"api/auth/signIn",
        REGISTER:"api/auth/signup",
        USER:"api/user/obtenerusuario",
        PRODUCTS:"api/products",
        PROFILE:"api/user"
    },
    STORAGE: {
        TOKEN: "token"
    }
}