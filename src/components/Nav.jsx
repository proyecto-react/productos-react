import React from 'react';
import { Link } from 'react-router-dom';
import {Layout,Menu} from 'antd';
const { Header } = Layout;
import imagenusuario from '../assets/images/imagen-usuario.png';
import './Nav.css'
import DrawerComponent from './Drawer';






const Nav = () => {

    const TabNames = ["","Products","Servicios","Contacto"];
    const items = TabNames.map((name, index) => ({
    key: index+1,
    label: name,
    url: index === 0 ? "/" : `/${name.toLowerCase()}`,
    }));


    return(
        <Header className='header-content'>
            <Link to="/">
            <img src={imagenusuario} alt='logo' style={{height:60,width:60}}></img>
            </Link>

            <Menu 
            theme='light'
            mode='horizontal'
            defaultSelectedKeys={['1']}
            style={{
                display: "flex",
                justifyContent: "flex-end",
                flex: 1,
                minWidth: 0,
                marginRight: '20px'
            }}
            >

                {items.map(item => (
                    <Menu.Item key={item.key}>
                        <Link to={item.url}>{item.label}</Link>
                    </Menu.Item>
                ))}

            </Menu>
            <DrawerComponent></DrawerComponent>
        </Header>
    );
}

export default Nav;