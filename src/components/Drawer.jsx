import React, { useState, useEffect } from 'react';
import { Drawer, Avatar, Button, Modal, Form, Input, message } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { useAuth } from '../hooks/useAuth';
import { userService } from '../services/users';
import imagenusuario from '../assets/images/imagen-usuario.png';

const DrawerComponent = () => {
    const [open, setOpen] = useState(false);
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [form] = Form.useForm();

    const showDrawer = () => {
        setOpen(true);
    };

    const onClose = () => {
        setOpen(false);
    };

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
        form.resetFields(); // Resetear el formulario al cerrar el modal
    };

    const handleOk = () => {
        form.submit();
    };

    const { user, logout, token } = useAuth();

    useEffect(() => {
        const fetchUserData = async () => {
            try {
                const userData = await userService.getMe(token);
                form.setFieldsValue(userData);
            } catch (error) {
                message.error('Error al obtener los datos del usuario');
            }
        };

        if (token) {
            fetchUserData();
        }
    }, [token, form]);

    const onFinish = async (values) => {
        try {
            await userService.editUser(user._id, values);
            message.success('Perfil actualizado con éxito');
            setIsModalVisible(false);
            form.resetFields(); // Resetear el formulario al cerrar el modal
        } catch (error) {
            message.error('Error al actualizar el perfil');
        }
    };

    return (
        <>
            <Avatar
                onClick={showDrawer}
                size={44}
                style={{ backgroundColor: '#87d068', cursor: 'pointer' }}
                icon={<UserOutlined />}
            />

            <Drawer title="Perfil de Usuario" onClose={onClose} open={open}>
                <h2 style={{color:'black'}}>Usuario: {user.username}</h2>
                <div style={{margin:0,display:'flex',alignItems:'center',justifyContent:'center'}}>
                
                <img src={imagenusuario} alt='logo' style={{height:170,width:170}}></img>
                </div>
                
                
                <div style={{paddingTop:130}}>
                <Button type="primary" onClick={showModal} style={{ marginBottom: 16,width:'100%' }}>
                    Editar Perfil
                </Button>
                <br></br>
                <Button style={{width:'100%'}} onClick={() => logout()}>Cerrar Sesión</Button>
                </div>
                
            </Drawer>

            <Modal
                title="Editar Perfil"
                visible={isModalVisible}
                onOk={handleOk}
                onCancel={handleCancel}
            >
                <Form form={form} onFinish={onFinish} layout="vertical">
                    <Form.Item
                        name="username"
                        label="Nombre de Usuario"
                        rules={[{ required: true, message: 'Por favor ingrese su nombre de usuario' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name="email"
                        label="Correo Electrónico"
                        rules={[{ required: true, message: 'Por favor ingrese su correo electrónico' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item name="password" label="Contraseña">
                        <Input.Password />
                    </Form.Item>
                </Form>
            </Modal>
        </>
    );
};

export default DrawerComponent;
