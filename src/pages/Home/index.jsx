import React from 'react';
import { Input,Button, Form } from 'antd'; 
import { useAuth } from '../../hooks/useAuth';
import ProductsMenu from '../products';
import DrawerComponent from '../../components/Drawer';
import Nav from '../../components/Nav';

const Home = () => {


    const useAuthData = useAuth();
    

    const { user,logout } = useAuthData;


    

    return(
        
        <>
    <div className="div-scroll" style={{ height: '600px', overflowY: 'scroll' }}>
        <Nav></Nav>
        
        
        <ProductsMenu></ProductsMenu>
        </div>
        </>
        
    );
}

export default Home;