import React, { useState, useEffect } from 'react';
import { Card, Col, Row, Button, Modal, Form, Input, message } from 'antd';
import { ExclamationCircleOutlined ,EditOutlined, DeleteOutlined,PlusOutlined } from '@ant-design/icons';
import { useNavigate } from 'react-router-dom';
import productsService from '../../services/products';
import { useAuth } from '../../hooks/useAuth';

const { confirm } = Modal;

const ProductsMenu = () => {
  const navigate = useNavigate();
  const [products, setProducts] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [currentProduct, setCurrentProduct] = useState(null);
  const [form] = Form.useForm();
  const useAuthData = useAuth();
  const { user } = useAuthData;
  const ADMIN_ROLE_ID = "65e68747162f507ddce3d5f3"; // ID del rol de administrador

  useEffect(() => {
    cargarProductos();
  }, []);

  const isAdmin = () => {
    if(user){
    return user.roles && user.roles.includes(ADMIN_ROLE_ID);
    }
  };

  const cargarProductos = async () => {
    try {
      const response = await productsService.getproducts();
      setProducts(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  const handleDelete = async (productId) => {
    try {
      await productsService.deleteProduct(productId);
      message.success('Producto eliminado');
      cargarProductos(); // Reload products after deletion
      
    } catch (error) {
      console.error('Error deleting product:', error);
    }
  };

  const showDeleteConfirm = (productId, productName) => {
    confirm({
      title: `¿Estás seguro que deseas eliminar "${productName}"?`,
      icon: <ExclamationCircleOutlined />,
      content: 'Esta acción no se puede deshacer.',
      okText: 'Eliminar',
      okType: 'danger',
      cancelText: 'Cancelar',
      onOk() {
        handleDelete(productId);
      },
    });
  };

  const showEditModal = (product) => {
    setCurrentProduct(product);
    form.setFieldsValue(product);
    setIsModalVisible(true);
  };

  const handleEdit = async (values) => {
    try {
      var nombreproducto = values.name.charAt(0).toUpperCase() + values.name.slice(1)
      await productsService.editProduct(currentProduct._id, nombreproducto, values.price, values.category, values.imgURL);
      setIsModalVisible(false);
      message.success('Producto editado');
      cargarProductos(); // Reload products after edit
    } catch (error) {
      console.error('Error editing product:', error);
    }
  };

  const ProductCard = ({ product }) => (
    <Col span={6} style={{ marginBottom: 20 }}>
      <Card
        hoverable
        cover={<img alt={product.name} src={product.imgURL} />}
        actions={[
          isAdmin() && (
            <>
            <div style={{padding:10}}>
              <Button icon={<EditOutlined />} type="text" style={{background:"#6BFBB7"}} onClick={() => showEditModal(product)}>
                Editar
              </Button>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <Button icon={<DeleteOutlined />} type="default" danger onClick={() => showDeleteConfirm(product._id, product.name)}>
                Eliminar
              </Button>
              </div>
            </>
          ),
        ]}
      >
        <Card.Meta style={{paddingBottom:10 }} title={product.name} />
        <Card.Meta style={{paddingBottom:10}}  title="Categoría:" description={`${product.category}`} />
        <Card.Meta style={{paddingBottom:10}}  title="Precio:" description={`$${product.price.toFixed(2)}`} />
      </Card>
    </Col>
  );

  return (
    <div style={{paddingTop:10, backgroundColor:'#363232'}} className="app">
      <h1 style={{color:"white", marginBottom: 16, marginLeft:20 }}>Menú de Productos</h1>
      {isAdmin() && (
        <Button icon={<PlusOutlined />} type="primary" style={{ marginBottom: 16, marginLeft:30 }} onClick={() => navigate('altaproducto')}>
          Añadir Producto
        </Button>
      )}
      <Row style={{ marginLeft:20, marginRight:20 }} gutter={50}>
        {products.map((product) => (
          <ProductCard key={product._id} product={product} />
        ))}
      </Row>
      <Modal
        title="Editar Producto"
        visible={isModalVisible}
        onCancel={() => setIsModalVisible(false)}
        onOk={() => form.submit()}
      >
        <Form form={form} onFinish={handleEdit} layout="vertical">
          <Form.Item name="name" label="Nombre" rules={[{ required: true, message: 'Por favor ingrese el nombre del producto' }]}>
            <Input />
          </Form.Item>
          <Form.Item name="price" label="Precio" rules={[{ required: true, message: 'Por favor ingrese el precio del producto' }]}>
            <Input type="number" />
          </Form.Item>
          <Form.Item name="category" label="Categoría" rules={[{ required: true, message: 'Por favor ingrese la categoría del producto' }]}>
            <Input />
          </Form.Item>
          <Form.Item name="imgURL" label="URL de la Imagen" rules={[{ required: true, message: 'Por favor ingrese la URL de la imagen del producto' }]}>
            <Input />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};

export default ProductsMenu;
