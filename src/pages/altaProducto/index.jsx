import React,{useState} from 'react';
import { Form,Input,Button,Card,message } from 'antd';
import { BoldOutlined, LockOutlined,DollarCircleFilled,FileImageOutlined } from '@ant-design/icons';
import axios from 'axios';
import './altaProductos.css'
import { useNavigate } from 'react-router-dom';
import productsService from '../../services/products';
import { useAuth } from '../../hooks/useAuth';



const FormAltaProductos = () => {
    const navigate = useNavigate();
    

    const [altaError, setAltaError ] = useState(false);

    const [loading, setLoading] = useState(false);//estado de carga

    const useAuthData = useAuth();
    console.log(useAuthData)

    const { user } = useAuthData;


    const ADMIN_ROLE_ID = "65e68747162f507ddce3d5f3"; // ID del rol de administrador

    const isAdmin = () => {
      user.roles && user.roles.includes(ADMIN_ROLE_ID) ? console.log("si es") : console.log("no es");
      return user.roles && user.roles.includes(ADMIN_ROLE_ID);
    };

    if(!isAdmin){
        navigate('/');
    }

    

    const onFinish = async (values) =>{
        setAltaError(false);
        setLoading(true);//establece el tiempo de carga
        try {
            var nombreproducto = values.name.charAt(0).toUpperCase() + values.name.slice(1)
            const response = await productsService.newproduct(nombreproducto,values.price,values.category,values.imgUrl);

            if (response && response.data) {
                
            
                message.success('Producto dado de alta');
                navigate("/");
            
            
            }else{
                console.error('Error en añadir el producto: Respuesta inesperada');
                message.error('El producto no pudo ser dado de alta');
                setAltaError(true);
            }
        } catch (error) {
            console.error('Error en el añadir producto', error.response ? error.response.data : error.message);
            message.error('El producto no pudo ser dado de alta');
            setAltaError(true);
        } finally {
            setLoading(false)//establece el tiempo de carga a false
        }
    }

    const onFinishFailed = (errorInfo) => {
        console.log('Failed',errorInfo);
        setAltaError(true);
    }

    return (
        <>
        
        <Card
        title="Alta Productos"
        bordered={false}
        className='responsive-card'
        >

            <Form
            name='normal_login'
            className='alta-product-form'
            initialValues={{
                remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            >
                <Form.Item
                name="name"
                rules={[
                    {
                    required: true,
                    message: 'Por favor ingrese Nombre'
                    }
                ]}
                >
                    <Input prefix = {<BoldOutlined/>} placeholder="Nombre de producto" />
                </Form.Item>
                

                <Form.Item
                name="price"
                rules={[
                    {
                    required: true,
                    message: 'Por favor ingrese su Precio'
                    }
                ]}
                >
                    <Input prefix = {<DollarCircleFilled/>} placeholder="Precio $$" />
                </Form.Item>



                <Form.Item
                name="category"
                rules={[
                    {
                    required: true,
                    message: 'Por favor ingrese Categoria'
                    }
                ]}
                >
                    <Input  placeholder="Categoria" />
                </Form.Item>


                <Form.Item
                name="imgUrl"
                rules={[
                    {
                    required: true,
                    message: 'Por favor ingrese Url'
                    }
                ]}
                >
                    <Input prefix = {<FileImageOutlined />} placeholder="Url de la imagen" />
                </Form.Item>



                <Form.Item>
                {altaError && <p style={{ color: 'red' }}>Fallo. Intentalo de nuevo</p>}
                    <Button type='primary' htmlType='submit' className='login-form-button' loading={loading}>
                        Dar de alta
                    </Button>
                </Form.Item>

                 <a href='/'>Volver</a>

            </Form>
        </Card>
        </>
    );
}

export default FormAltaProductos