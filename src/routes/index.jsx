import React from 'react';
import { useRoutes } from 'react-router-dom'
import Home from '../pages/Home'
import Login from '../pages/Login'
import FormAltaProductos from '../pages/altaProducto';
import Register from '../pages/Register';
import { useAuth } from '../hooks/useAuth';
import Products from '../pages/products';



const AppRoutes = () => {
    
    const { user } = useAuth();

    let routes =  useRoutes([
        {path: '/', element: user ? <Home/> : <Login/> },
        {path: '/productos',element: <div style={{ height: '600px', overflowY: 'scroll' }}><Products/></div> },
        {path: '/altaproducto', element: user ? <FormAltaProductos/> : <Login/> },
        {path: '/login', element:<Login/>},
        {path: '/register', element:<Register/>}
    ])
    return routes;

}

export default AppRoutes;